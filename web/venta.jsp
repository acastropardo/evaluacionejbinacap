<%-- 
    Document   : venta.jsp
    Created on : 05-jun-2021, 14:28:26
    Author     : abrah
--%>

<%@page import="javax.naming.InitialContext"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="modelo.SessionBeanServicioLocal"%>
<%@page import="modelo.Producto"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!SessionBeanServicioLocal servicio;%>

<%
    InitialContext ctx = new InitialContext();
    servicio = (SessionBeanServicioLocal) ctx.lookup("java:global/EvaluacionAbrahamCastro/SessionBeanServicio!modelo.SessionBeanServicioLocal");
%>
<c:set var="servicio" scope="page" value="<%=servicio%>"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="./jquery-3.6.0.min.js"></script>
        
        <script>
            function invocaAJAX() {
                var cod=document.getElementById("cod").value;
                var cant=document.getElementById("cant").value;
                
                $.ajax({
                    type: "post",
                    url: "control.do",
                    data: {"cod": cod, "cant":cant},
                    success: function (msg) {
                        alert(msg.data);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(xhr.status);
                        alert(thrownError);
                    }
                });
                return false;
            }
        </script>
    </head>
    <body>
        <form action="control.do" method="POST">
            <jsp:include page="cabecera.jsp"/>
            <h1>Detalle Venta</h1>
            <br>
            <br>
            <h3>
                Codigo: ${param.codigo} <br>
                <%  int codigoint = Integer.parseInt(request.getParameter("codigo"));
                    Producto producto = servicio.buscarProducto(codigoint); %>
                Nombre: <% out.println(producto.getNombre());%><br>
                Precio: $<% out.println(producto.getPrecio());%><br>
                Cantidad: <input type="text" name="cant" id="cant">
                <input type="hidden" name="cod" id="cod" value="${param.codigo}">
            </h3>
            <input type="submit" value="Comprar" name="bt">
            <input id = "AJAX" type = "hidden" value = "prueba" onclick = "invocaAJAX();" />

        </form>
    </body>
</html>
