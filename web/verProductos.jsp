<%-- 
    Document   : verProductos.jsp
    Created on : 05-jun-2021, 13:20:33
    Author     : abrah
--%>


<%@page import="javax.naming.InitialContext"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="modelo.SessionBeanServicioLocal"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!SessionBeanServicioLocal servicio;%>

<%
InitialContext ctx= new InitialContext();
servicio=(SessionBeanServicioLocal) ctx.lookup("java:global/EvaluacionAbrahamCastro/SessionBeanServicio!modelo.SessionBeanServicioLocal");
%>
<c:set var="servicio" scope="page" value="<%=servicio%>"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="cabecera.jsp"/>
        <h1>Listado de Productos</h1>
        <table border="1">
                <tr>
                    <td>Código</td>
                    <td>Nombre</td>
                    <td>Precio</td>
                    <td>Stock</td>
                    <td>Categoria</td>
                </tr>
                <c:forEach items="${servicio.getProductos()}" var="p">
                <tr>
                    <td>${p.codigo}</td>
                    <td>${p.nombre}</td>
                    <td>${p.precio.toString()}</td>
                    <td>${p.stock.toString()}</td>
                    <td>${p.categoria.getNombre()}</td>
                    
                </tr>
                </c:forEach>
        </table>
    </body>
</html>
