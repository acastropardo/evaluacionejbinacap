/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import javax.ejb.Local;

/**
 *
 * @author abrah
 */
@Local
public interface SessionBeanServicioLocal {
    
    public Producto buscarProducto(int codigo);
    
    public ArrayList<Producto> getProductos();
    
    public String Vender(int codigo, int cantidad);
}
