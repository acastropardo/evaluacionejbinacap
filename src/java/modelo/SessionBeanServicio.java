/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import javax.ejb.Singleton;

/**
 *
 * @author abrah
 */
@Singleton
public class SessionBeanServicio implements SessionBeanServicioLocal {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    private ArrayList<Producto> listaProducto = new ArrayList();

    public SessionBeanServicio() {
        listaProducto.add(new Producto(1, "Mouse", 3000, 18, new Categoria(1, "Dispositivo de Entrada", "")));
        listaProducto.add(new Producto(2, "Teclado", 5000, 25, new Categoria(1, "Dispositivo de Entrada", "")));
        listaProducto.add(new Producto(3, "Pendrive 8GB", 4000, 10, new Categoria(2, "Almacenamiento", "")));
        listaProducto.add(new Producto(4, "HDD Externo 500GB", 50000, 6, new Categoria(2, "Almacenamiento", "")));
        listaProducto.add(new Producto(5, "Kit de Limpieza", 3000, 20, new Categoria(3, "Utilidades", "")));
        listaProducto.add(new Producto(6, "Cámara GoPro HD", 120000, 23, new Categoria(4, "Multimedia", "")));

    }

    @Override
    public Producto buscarProducto(int codigo) {
        for (Producto p : listaProducto) {
            if (p.getCodigo() == codigo) {
                return p;
            }

        }
        return null;
    }

    @Override
    public ArrayList<Producto> getProductos() {
        return listaProducto;
    }

    @Override
    public String Vender(int codigo, int cantidad) {
        int contadorStock = 0;
        int precio = 0;
        Integer price = 0;

        String salida;
        salida = "";

        if (cantidad > 0) {

            for (Producto p : listaProducto) {
                if (p.getCodigo() == codigo) {
                    contadorStock = p.getStock();
                }
            }

            if (contadorStock >= cantidad) {

                for (Producto p : listaProducto) {
                    if (p.getCodigo() == codigo) {
                        precio = p.getPrecio();
                        price = new Integer(precio) * cantidad;
                    }
                }

                if (precio > 0) {
                    salida = price.toString();
                }
                return salida;
            } else {
                return "No hay stock de ese producto";
            }
        } else {
            return "La cantidad no puede ser 0";
        }
    }

}
